//
//  PhotoViewController.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-28.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Members
    
    /// the photo index for this view controller
    var index: Int!
    
    /// Set if the another object will take care of the photo loading for us
    var manualPhoto = false
    
    /// The photo information associated with this view controller
    var photoInfo: PhotoInfo? = nil
    
    /// The current zoom scale for the scroll view
    var zoomScale: CGFloat {
        return scrollView.zoomScale
    }

    // MARK: - Initialization
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // If image is already set, we are already set up
        guard imageView.image == nil else {
            return
        }
        if let info = photoInfo, !manualPhoto {
            progressIndicator.startAnimating()
            loadPhoto(info: info)
        }
    }
    
    // MARK: - Image related

    /**
     Set the photo of this view manually. Also stops the progress indicator.
     - parameter image: the image to set
    */
    func setPhotoImage(image: UIImage) {
        progressIndicator.stopAnimating()
        imageView.image = image
        prepareImage()
    }
    
    /**
     Show or hide image
     - parameter show: if the image should be shown or hidden
     */
    func showImage(_ show: Bool) {
        imageView.isHidden = !show
    }
    
    /**
     Load a new photo
     - parameter info: the photo information
     */
    func loadPhoto(info: PhotoInfo) {
        UIImage.loadAsynchronousImage(url: info.url) {(error, image, cache) in
            DispatchQueue.main.async {
                self.progressIndicator.stopAnimating()
                guard error == nil else {
                    showOkDialog(on: self, message: "ERROR_DOWNLOADING_IMAGE".localized())
                    return
                }
                if cache {
                    self.imageView.image = image
                } else {
                    self.imageView.fadeInImage(image: image, duration: imageFadeInDuration)
                }
                self.prepareImage()
            }
        }
    }

    // MARK: - Zoom funtionality
    
    /// Zoom out image to fit screen
    func zoomOut() {
        scrollView.setZoomScale(1, animated: true)
    }
    
    /// Zoom in image
    func zoomIn() {
        scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
    }
    
    // MARK: Scroll view delegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    // Center the image on zoom
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerImageView(withSize: imageView.frame.size, containerSize: scrollView.bounds.size)
    }
    
    // MARK: - Private helper functions
    
    // Center the image by changing the scroll view content insets
    private func centerImageView(withSize imageViewSize: CGSize, containerSize: CGSize) {
        
        let centerX = imageViewSize.width < containerSize.width
        let centerY = imageViewSize.height < containerSize.height
        
        scrollView.contentInset.left = centerX ? (containerSize.width - imageViewSize.width) / 2 : 0
        scrollView.contentInset.top = centerY ? (containerSize.height - imageViewSize.height) / 2 : 0
    }
    
    // Do initial setup when we load a new photo
    private func prepareImage() {
        setZoomLevel()
        setUpImageViewWithContainerSize(scrollView.bounds.size)
    }
    

    //  Set new max zoom scale for scroll view. The maximum zoom scale depends on the resolution of the image in the image view. A image of higher resolution allows for more zoom.
    private func setZoomLevel() {
        guard let image = imageView.image else {
            scrollView.maximumZoomScale = 1
            return
        }
        
        // Max zoom level will depend only on the smallest dimension. This means that the image might be zoomed in further
        // if we change orientation (this is also the behavior of the Photos app).
        let portraitWidth = min(scrollView.bounds.size.width, scrollView.bounds.size.height)
        let imageWidthPixels = image.size.width * image.scale
        let widthPixels = portraitWidth * UIScreen.main.scale
        
        // Allow zooming in to 2:1 with regards to screen resolution
        // Always set minimum scale to a fraction higher than 1 to allow for zoom bouncing on
        // images with low resolution
        let newScale = max(1.000000001, 2*imageWidthPixels / widthPixels)
        scrollView.maximumZoomScale = newScale
    }
    
    // Set new image size for the given container size and center the image
    private func setUpImageViewWithContainerSize(_ containerSize: CGSize) {
        guard let image = imageView.image else {return}
        let newFullSize = sizeForFullImage(image, containedInSize: containerSize)
        imageViewWidthConstraint.constant = newFullSize.width
        imageViewHeightConstraint.constant = newFullSize.height
        
        // Multiply by current zoom scale to get the size of the zoomed in image
        // We could also use scrollView.layoutIfNeeded() here and then get the size via imageView.frame.size,
        // but calculating manually should be more efficient
        let zoomedImageSize = CGSize(width: newFullSize.width*scrollView.zoomScale, height: newFullSize.height * scrollView.zoomScale)
        
        centerImageView(withSize: zoomedImageSize, containerSize: containerSize)
    }
    
    // Adjust image when we rotate
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            self.setUpImageViewWithContainerSize(size)
            self.scrollView.layoutIfNeeded()
        }, completion: nil)
    }
}
