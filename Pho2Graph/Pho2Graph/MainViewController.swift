//
//  ViewController.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-28.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import UIKit

import UnsplashKit

class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Members
    private let unsplashManager = UnsplashManager()
    private var lastSearch: String!
    private var photoSize: UnsplashManager.PhotoSize = .regular
    private var photoSizeThumb: UnsplashManager.PhotoSize = .thumb
    
    // MARK: - Collecion View data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return unsplashManager.numberOfPhotos
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
        
        // Use tag for a simple cell such as this. Should be done with a subclass if cell is more complicated
        let imageView = imageCell.contentView.viewWithTag(5) as! UIImageView
        
        let index = indexPath.item
        
        imageView.image = nil
        if let info = unsplashManager.photoInfo(at: index, size: photoSizeThumb) {
            UIImage.loadAsynchronousImage(url: info.url) {(error, image, cache) in
                guard error == nil else {return}
                DispatchQueue.main.async {
                    if cache {
                        imageView.image = image
                    } else {
                        imageView.fadeInImage(image: image, duration: imageFadeInDuration)
                    }
                }
            }
        }
        
        // Check if we should fetch more data.
        // Don't fetch new page if we are presenting the photo viewer (it will take care of the fetching)
        let currentNumberOfPhotos = unsplashManager.numberOfPhotos
        if index == currentNumberOfPhotos - fetchWhenLeft, unsplashManager.hasNextPage, presentedViewController == nil {
            unsplashManager.fetchNextPage() {(error, newPhotos) in
                DispatchQueue.main.async {
                    guard error == nil else {
                        showOkDialog(on: self, message: "ERROR_DOWNLOADING_PAGE".localized())
                        return
                    }
                    
                    self.collectionView.insertItems(at: (currentNumberOfPhotos..<self.unsplashManager.numberOfPhotos).map {IndexPath(item: $0, section: 0)})
                }
            }
        }
        
        return imageCell
    }

    // MARK: - Collecion View delegate
    
    // Recalculate sizes on orientation change
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width
        let sideSize = width / CGFloat((width > 600 ? cellsPerRowLandscape : cellsPerRowPortrait))
        return CGSize(width: sideSize, height: sideSize)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchBar.endEditing(true)
        performSegue(withIdentifier: "photoScrollerSegue", sender: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "loadingFooter", for: indexPath) as! FooterCollectionReusableView
            
            // If we did not yet search, don't show footer
            if unsplashManager.didNotSearch {
                footer.isHidden = true
                return footer
            }
            
            footer.isHidden = false
            
            if unsplashManager.hasNextPage {
                footer.indicator.isHidden = false
                footer.label.isHidden = true
            } else {
                footer.label.isHidden = false
                footer.indicator.isHidden = true
                footer.label.text = unsplashManager.hasResults ? "NO_MORE_RESULTS".localized() : "NO_RESULTS".localized()
            }
            return footer
        }
        fatalError("Unhandled supplementary view")
    }
    
    // MARK: - Scroll View delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Remove keyboard when we start scrolling
        if scrollView == collectionView {
            searchBar.endEditing(true)
        }
    }
    
    // MARK: - Search bar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // Delayed search
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.search), object: lastSearch)
        self.perform(#selector(self.search), with: searchText, afterDelay: searchDelay)
        lastSearch = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func search(searchText: String) {
        var searchText = searchText.trimWhitespace()
        guard !searchText.isEmpty else {
            return
        }
        
        var searchFun = unsplashManager.search
        
        // If user search
        if let username = searchText.removePrefix(prefix: userPrefix) {
            if username.isEmpty {
                return
            }
            searchFun = unsplashManager.user
            searchText = username
        }
        
        unsplashManager.removeAllPages()
        collectionView.reloadData()
        activityIndicator.startAnimating()
        
        searchFun(searchText) { (error, newPhotos) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                guard error == nil else {
                    showOkDialog(on: self, message: "ERROR_SEARCHING".localized())
                    return
                }
                self.collectionView.reloadData()
            }
        }
    }
    
    // MARK: - Interface related
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        // Recalculate cell size on orientation change
        flowLayout.invalidateLayout()
    }
    
    // MARK: - Navigation
    
    private func setupPhotoPageViewController(viewController: PageViewController, startIndexPath: IndexPath) {
        viewController.modalPresentationCapturesStatusBarAppearance = true
        viewController.startIndex = startIndexPath.item
        viewController.unsplashManager = unsplashManager
        viewController.photoSize = photoSize

        let cell = self.collectionView.cellForItem(at: startIndexPath)!
        
        // Show pop up effect on the view controller when it has downloaded the image. Close it if the image fails to download.
        viewController.didFinishLoadingImage = {[unowned self, unowned viewController] image in
            DispatchQueue.main.async {
                guard let image = image else {
                    viewController.dismiss(animated: false, completion: {
                        showOkDialog(on: self, message: "ERROR_DOWNLOADING_IMAGE".localized())
                    })
                    return
                }
                cell.isHidden = true
                viewController.popUpEffect(fromView: cell, image: image) {
                    cell.isHidden = false
                }
            }
        }
        
        // Pop down effect on close
        viewController.shouldClose = {[unowned self, unowned viewController] in
            let indexPath = IndexPath(item: viewController.currentPhotoIndex, section: 0)
            
            // Reload collection view if we fetched new photos in the photo viewer
            if viewController.didFetchNewPhotos {
                self.collectionView.reloadData()
                self.collectionView.layoutIfNeeded()
            }
            
            // Perform pop down if cell can be found
            let performPopDown = {() -> Bool in
                guard let cell = self.collectionView.cellForItem(at: indexPath) else {return false}
                    cell.isHidden = true
                    viewController.popDownEffect(toView: cell, completion: {
                        cell.isHidden = false
                        viewController.dismiss(animated: false, completion: nil)
                    })
                return true
            }
            
            // Try to perform pop down without scrolling collection view
            if !performPopDown() {
                // If cell couldn't be found, try scrolling to it and try again
                self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
                self.collectionView.layoutIfNeeded()
                if !performPopDown() {
                    // If we still can't find the cell, just close the photo viewer
                    viewController.dismiss(animated: true, completion: nil)
                }
            }
        }
        
        // When credit is touched, close photo viewer and search for the user
        viewController.didTouchCredits = {[unowned self, unowned viewController] username in
            if let username = username {
                let searchQuery = "\(userPrefix)\(username)"
                self.searchBar.text = searchQuery
                self.search(searchText: searchQuery)
                viewController.fadeOutViewController(duration: photoPopUpEffectAnimationDuration, completion: {
                    viewController.dismiss(animated: false, completion: nil)
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let photoPageViewController = segue.destination as? PageViewController {
            setupPhotoPageViewController(viewController: photoPageViewController, startIndexPath: sender as! IndexPath)
        }
    }
}

