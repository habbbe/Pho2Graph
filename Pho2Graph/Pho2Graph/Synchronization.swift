//
//  Synchronization.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-30.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import Foundation

/**
 Critical section protected by a mutex
 - parameter mutex: the mutex to lock
 - parameter code: the synchronized code
 - returns: what is returned in the code block
 */
func critical<T>(mutex: inout pthread_mutex_t, code: (Void) -> T) -> T {
    pthread_mutex_lock(&mutex)
    defer {pthread_mutex_unlock(&mutex)}
    return code()
}
