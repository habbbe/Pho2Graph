//
//  AsyncImageLoading.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-28.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    /**
     Load an image from an URL asynchronously
     - parameter url: the URL to load the image from
     - parameter completion: completion block
     */
    public static func loadAsynchronousImage(url: URL, completion: ((Error?, UIImage?, Bool) -> Void)? = nil) {
        let request = URLRequest(url: url)
        DispatchQueue.global(qos: .background).async {
            
            // Check if we have the image in cache
            if let response = URLCache.shared.cachedResponse(for: request) {
                let image = UIImage(data: response.data)
                completion?(nil, image, true)
                return
            }
            
            URLSession.shared.dataTask(with: request) {data,response,error in
                var image: UIImage? = nil
                if error == nil {
                    image = UIImage(data: data!)
                }
                completion?(error, image, false)
                }.resume()
        }
    }
}
