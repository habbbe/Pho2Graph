//
//  Utils.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-30.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import Foundation
import UIKit

/** 
 Show a simple dialog with OK button
 
 - parameter on: The view controller on which to present the dialog
 - parameter title: The title
 - parameter message: The message
 
 */
func showOkDialog(on viewController: UIViewController, title: String? = nil, message: String? = nil) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: nil))
    viewController.present(alert, animated: true, completion: nil)
}

/**
 
 Get a view controller from storyboard
 
 - parameter id: The view controller's storyboard id
 - returns: The view controller with the specified id
 
 */
func getViewController(id: String) -> UIViewController {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    return storyboard.instantiateViewController(withIdentifier: id)
}

/// Calculate the bounds of a completely zoomed out image
func sizeForFullImage(_ image: UIImage, containedInSize: CGSize) -> CGSize {
    let frameWidth = containedInSize.width
    let frameHeight = containedInSize.height
    let frameRatio = frameWidth / frameHeight
    let imageRatio = image.size.width / image.size.height
    let fillWidth = frameRatio < imageRatio
    let resultWidth = fillWidth ? frameWidth : frameHeight * imageRatio
    let resultHeight = fillWidth ? frameWidth / imageRatio : frameHeight
    return CGSize(width: resultWidth, height: resultHeight)
}

extension UIImageView {
    
    /**
     Fade in image
     - parameter image: the image to fade in
     - parameter duration: the duration of the fade
     */
    func fadeInImage(image: UIImage?, duration: TimeInterval) {
        UIView.transition(with: self, duration: duration, options: .transitionCrossDissolve, animations: {
            self.image = image
        }, completion: nil)
    }
}
