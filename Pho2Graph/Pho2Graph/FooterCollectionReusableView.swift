//
//  FooterCollectionReusableView.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-30.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import UIKit

class FooterCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
}
