//
//  StringExtensions.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-30.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import Foundation

extension String {
    
    /**
     Returns a localized string
     - parameter comment: comment
     - returns: the localized string
     */
    func localized(comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }
    
    /**
     Removes prefix from string and returns the rest
     - parameter prefix: the prefix to be removed
     - returns: the string with the prefix removed, or nil if the prefix was not found
     */
    func removePrefix(prefix: String) -> String? {
        if self.hasPrefix(prefix) {
            let index = self.index(self.startIndex, offsetBy: prefix.characters.count)
            return self.substring(from: index)
        }
        return nil
    }
    
    /// - returns: a new string with whitespace trimmed at beginning and end
    func trimWhitespace() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}
