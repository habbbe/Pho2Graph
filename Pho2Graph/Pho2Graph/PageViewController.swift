//
//  PageViewController.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-28.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import UIKit

class PageViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    // MARK: - Enums
    
    private enum Direction: Int {
        case forward = 1
        case backward = -1
        case stay = 0
    }
    
    // MARK: - Outlets
    @IBOutlet weak var creditsButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var singleTapRecognizer: UITapGestureRecognizer!
    @IBOutlet var doubleTapRecognizer: UITapGestureRecognizer!
    
    // MARK: - Members
    var unsplashManager: UnsplashManager!
    var photoSize: UnsplashManager.PhotoSize!
    var startIndex: Int!
    var didFetchNewPhotos = false
    private var pageViewController: UIPageViewController!
    private var currentViewController: PhotoViewController!
    let popUpImageView = UIImageView()
    var currentPhotoIndex: Int {
        return currentViewController.index
    }
    
    // Status bar
    var hideStatusBar: Bool = false
    override var prefersStatusBarHidden: Bool {return hideStatusBar}
    override var preferredStatusBarStyle: UIStatusBarStyle {return .lightContent}
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {return .fade}
    
    // MARK: - Completion handlers
    
    /// Called when the close button is touched
    var shouldClose: ((Void) -> Void)?
    
    /// Called when the first image has finished loading
    var didFinishLoadingImage: ((UIImage?) -> Void)?
    
    /// Called when the credits button is touched
    var didTouchCredits: ((String?) -> Void)?
    
    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set blurred border for OSD elements
        for view in [creditsButton, closeButton] as [UIView] {
            let layer = view.layer
            layer.shadowOpacity = 1
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowRadius = 10
        }
        
        // Set up pop up view
        popUpImageView.contentMode = .scaleAspectFill
        popUpImageView.clipsToBounds = true

        // Insert popUpView below OSD
        view.insertSubview(popUpImageView, belowSubview: creditsButton)
        
        // Make everything transparent (will be faded in with pop up effect)
        view.backgroundColor = UIColor.black.withAlphaComponent(0)
        setOSDAlpha(0)
        showOSD(false)
        
        // Set up tap recognizers
        singleTapRecognizer.require(toFail: doubleTapRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let info = unsplashManager.photoInfo(at: startIndex, size: photoSize)!
        currentViewController.photoInfo = info
        
        // Fetch the first image
        UIImage.loadAsynchronousImage(url: info.url) {(error, image, cache) in
            self.didFinishLoadingImage?(image)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonTouched(_ sender: UIButton) {
        shouldClose?()
    }
    
    @IBAction func creditsButtonTouched(_ sender: UIButton) {
        let username = currentViewController.photoInfo?.username
        didTouchCredits?(username)
    }
    
    // Hide OSD on single tap
    @IBAction func singleTapped(_ sender: Any) {
        UIView.transition(with: self.view, duration: photoPopUpEffectAnimationDuration, options: .transitionCrossDissolve, animations: {
            for view in [self.closeButton, self.creditsButton] as [UIView] {
                view.isHidden = !view.isHidden
            }
            self.hideStatusBar = !self.hideStatusBar
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
    
    // Zoom out image on double tap
    @IBAction func doubleTapped(_ sender: Any) {
        if currentViewController.zoomScale == 1 {
            currentViewController.zoomIn()
        } else {
            currentViewController.zoomOut()
        }
    }
    
    // MARK: - Page view controller data source

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let oldViewController = viewController as! PhotoViewController
        return nextViewController(direction: .forward, index: oldViewController.index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let oldViewController = viewController as! PhotoViewController
        return nextViewController(direction: .backward, index: oldViewController.index)
    }
    
    // MARK: - Page view controller delegate

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let previous = previousViewControllers.first as? PhotoViewController,
              completed else {
            return
        }
        
        let current = pageViewController.viewControllers![0] as! PhotoViewController
 
        previous.scrollView.setZoomScale(1, animated: false)
        setCredits(name: current.photoInfo?.userFullName)
        currentViewController = current
    }
    
    // MARK: - Pop up effect
    
    /*
     The pop up/down effect is performed in the following way:
     We have an image view that will contain our image. This image view will always show the whole image, but will be clipped due to contentMode == .scaleAspectFill.
     The image view will then transform into the final size and aspect ratio, revealing all the parts of the image.
     
     The final position and size of the image is calculated from the dimensions of the view that will later contain the full image (in this case containerView).
     
     For the pop down a similar approach is taken
     */
    
    /**
     Do a pop up effect for a view and image.
     After the animation is finished, the image will be re-used in the initial photo view
     
     - parameter fromView: the view that will be "popped up" from
     - parameter image: the image to use in the pop up
     - parameter completion: completion handler called when the animation has finished
    */
    func popUpEffect(fromView: UIView, image: UIImage, completion: ((Void) -> Void)? = nil) {
        
        popUpImageView.image = image
        // Show the OSD (now alpha == 0)
        showOSD(true)
        
        // Calculate location for minimized cell in new view
        let oldFrame = fromView.superview!.convert(fromView.frame, to: self.view)
        popUpImageView.frame = oldFrame
        
        // Calculate new frame
        let resultSize = sizeForFullImage(image, containedInSize: containerView.bounds.size)
        
        UIView.transition(with: self.view, duration: photoPopUpEffectAnimationDuration, options: [.curveEaseInOut], animations: {
            self.popUpImageView.frame.size = resultSize
            self.popUpImageView.center = self.containerView.center
            self.view.backgroundColor = UIColor.black
            self.setOSDAlpha(1)
        }, completion: {success in
            self.currentViewController.setPhotoImage(image: image)
            self.currentViewController.showImage(true)
            self.popUpImageView.isHidden = true
            completion?()
        })
    }
    
    /**
     Do a pop down effect to a view
     
     - parameter toView: the view that will be "popped down" to
     - parameter completion: completion handler called when the animation has finished
     */
    func popDownEffect(toView: UIView, completion: ((Void) -> Void)? = nil) {
        
        // Final frame (the location of the view to "pop down" to)
        let newFrame = toView.superview!.convert(toView.frame, to: self.view)
        
        // Set image for pop down view (or no image if not yet loaded)
        let maybeImage = currentViewController.imageView.image
        popUpImageView.image = maybeImage

        popUpImageView.frame.size = { () -> CGSize in
            if let image = maybeImage {
                var finalSize = sizeForFullImage(image, containedInSize: containerView.bounds.size)
                
                // Multiply by current zoom scale in photo view to get size of zoomed in image
                let zoomScale = currentViewController.zoomScale
                finalSize.width *= zoomScale
                finalSize.height *= zoomScale
                return finalSize
            } else {
                // If image is not loaded, just use a square for the frame
                let side = min(containerView.bounds.width, containerView.bounds.height)
                return CGSize(width: side, height: side)
            }}()
        
        // Convert the center point for the shown image to our view
        popUpImageView.center = currentViewController.imageView.superview!.convert(currentViewController.imageView.center, to: self.view)
        
        self.currentViewController.showImage(false)
        popUpImageView.isHidden = false

        UIView.transition(with: self.view, duration: photoPopUpEffectAnimationDuration, options: [.curveEaseInOut], animations: {
            self.popUpImageView.frame = newFrame
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
            self.setOSDAlpha(0)
        }, completion: {success in
            self.popUpImageView.isHidden = true
            completion?()
        })
    }
    
    /**
     Fade out the main view for the view controller
     - parameter duration: the duration of the fade
     - parameter completion: block called when the animation is completed
     */
    func fadeOutViewController(duration: TimeInterval, completion: ((Void) -> Void)?) {
        UIView.animate(withDuration: duration, animations: { 
            self.view.alpha = 0
        }) {completed in
            completion?()
        }
    }
    
    // MARK: - Private helpers
    
    /**
     Return the next view controller for paging
     
     - parameter direction: the direction in which to page
     - parameter index: the index for the photo to load
     
     - returns: the next view controller
     */
    private func nextViewController(direction: Direction, index: Int) -> PhotoViewController? {
        let nextIndex = index + direction.rawValue
        
        guard (direction == .stay ||
               direction == .forward && nextIndex < unsplashManager.numberOfPhotos ||
               direction == .backward && nextIndex >= 0) else {
            return nil
        }
        
        let viewController = getViewController(id: "PhotoViewController") as! PhotoViewController
        viewController.index = nextIndex
        
        // Check if we are at the last fetched photo and have another page to fetch
        let shouldFetch = direction == .forward && nextIndex == unsplashManager.numberOfPhotos - fetchWhenLeft - 1 && unsplashManager.hasNextPage
        if shouldFetch {
            // Fetch next page
            unsplashManager.fetchNextPage() {(error, newPhotos) in
                DispatchQueue.main.async {
                    guard error == nil else {
                        showOkDialog(on: self, message: "ERROR_DOWNLOADING_PAGE".localized())
                        return
                    }
                    self.didFetchNewPhotos = true
                    // Set photo in view controller manually
                    if let info = self.unsplashManager.photoInfo(at: nextIndex, size: self.photoSize) {
                        viewController.photoInfo = info
                        viewController.loadPhoto(info: info)
                    }
                }
            }
        } else {
            viewController.photoInfo = unsplashManager.photoInfo(at: nextIndex, size: photoSize)
        }
        // If we are on the first photo we will not let the child view controller load its own
        // image. This is because we take care of the loading when we do the pop up effect
        
        return viewController
    }
    
    private func setCredits(name: String?) {
        let newName = name ?? "UNKNOWN".localized()
        UIView.performWithoutAnimation {
            creditsButton.setTitle("\("PHOTO_BY".localized()) \(newName)", for: .normal)
            creditsButton.layoutIfNeeded()
        }
    }
    
    private func setOSDAlpha(_ alpha: CGFloat) {
        for view in [closeButton, creditsButton] as! [UIView] {
            view.alpha = alpha
        }
    }
    
    private func showOSD(_ show: Bool) {
        for view in [closeButton, creditsButton] as! [UIView] {
            view.isHidden = !show
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Set up page view controller when it is first shown
        if let pageViewController = segue.destination as? UIPageViewController {
            self.pageViewController = pageViewController
            pageViewController.dataSource = self
            pageViewController.delegate = self
            currentViewController = nextViewController(direction: .stay, index: startIndex)!
            
            // Tell first view controller that we will take care of the loading of the picture
            currentViewController.manualPhoto = true
            let info = unsplashManager.photoInfo(at: startIndex, size: .thumb)
            setCredits(name: info?.userFullName)
            pageViewController.setViewControllers([currentViewController], direction: .forward, animated: false, completion: nil)
        }
    }
}
