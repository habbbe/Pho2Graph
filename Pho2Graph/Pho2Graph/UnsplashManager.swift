//
//  UnsplashManager.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-30.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import Foundation
import UnsplashKit

typealias PhotoInfo = (url: URL, userFullName: String, username: String?)

class UnsplashManager {
    
    typealias PhotosCompletion = ((Error?, Int) -> Void)
    
    enum PhotoSize {
        case thumb, small, regular, full, raw
    }
    
    // MARK: - Members
    
    private var resourcesMutex = pthread_mutex_t()
    private var responses = [Response<[Photo]>]()
    private var photos = [Photo]()
    private let unsplashClient: UnsplashClient
    
    // MARK: - Initialization
    
    init() {
        if let infoPlist = Bundle.main.infoDictionary,
            let unsplashClientId = infoPlist["Unsplash client id"] {
            unsplashClient = UnsplashClient { _ in
                return [ "Authorization": "Client-ID \(unsplashClientId)"]
            }
        } else {
            fatalError("Key \"Unsplash client id\" not found in Info.plist")
        }
    }
    
    // MARK: - Fetching data
    
    /**
     
     Search Unsplash for a string
     
     - parameter searchText: The string to search for
     - parameter completion: Completion closure
     
     */
    func search(searchText: String, completion: PhotosCompletion?) {
        guard !searchText.isEmpty else {
            return
        }
        
        executeUnsplash(resource: Search.photos(query: searchText, perPage: photosPerPage), completion: completion)
    }
    
    /**
     
     Search Unsplash for a string
     
     - parameter searchText: The string to search for
     - parameter completion: Completion closure
     
     */
    func user(username: String, completion: PhotosCompletion?) {
        guard !username.isEmpty else {
            return
        }
        
        executeUnsplash(resource: User.photos(username: username, perPage: photosPerPage), completion: completion)
    }
    
    /**
     
     Fetch next page for current search
     
     - parameter completion: Completion closure
     
     */
    func fetchNextPage(completion: PhotosCompletion?) {
        guard let nextPageLink = getNextPage() else {
            return
        }
        
        executeUnsplash(resource: Search.followLink(link: nextPageLink), completion: completion)
    }
    
    // MARK: - Querying information about state
    
    /// If a search has been made or not
    var didNotSearch: Bool {
        return responses.isEmpty
    }

    /// If a search has any results
    var hasResults: Bool {
        if let responses = responses.last {
            return !responses.object.isEmpty
        }
        return false
    }
    
    /// If a search has an unfetched page
    var hasNextPage: Bool {
        return getNextPage() != nil
    }
    
    /// Current number of photos fetched
    var numberOfPhotos: Int {
        return photos.count
    }
    
    /**
     Get photo information via index
     
     - parameter at: index of photo
     - parameter size: the photo size
     - returns: a pair (`url`, `credits`) where url is the `url` to the image and `credits` is the name of the photographer, or `nil` if there is no urls for the photo
     
     */
    func photoInfo(at index: Int, size: PhotoSize) -> PhotoInfo? {
        let photo = photos[index]
        guard let photoUrls = photo.urls else {
            return nil
        }
        let urlString: String = {
        switch size {
        case .thumb:
            return photoUrls.thumb
        case .small:
            return photoUrls.small
        case .regular:
            return photoUrls.regular
        case .full:
            return photoUrls.full
        case .raw:
            return photoUrls.raw
        }
        }()
        
        var name = "UNKNOWN".localized()
        if let user = photo.user {
            name = user.firstName
            if let lastName = user.lastName {
                name.append(" \(lastName)")
            }
        }
        
        return (URL(string: urlString)!, name, photo.user?.username)
    }
    
    // MARK: - State management
    
    /// remove all fetched pages
    func removeAllPages() {
        removeAllResponses()
    }
    
    
    // MARK: - Private helpers
    
    /// - returns: link to next page
    private func getNextPage() -> Link? {
        return responses.last?.nextLink
    }
    
    // Execute an Unsplash call
    private func executeUnsplash(resource: Resource<[Photo]>, completion: ((Error?, Int) -> Void)?) {
        unsplashClient.execute(resource: resource) { result in
            switch result {
            case .success(let response):
                print("Fetched \(response.object.count) photos")
                self.addResponse(at: self.responses.count, response: response)
                completion?(nil, response.object.count)
            case .failure(let error):
                completion?(error, 0)
            }
        }
    }
    
    // MARK: Resource management
    
    // Add new response
    private func addResponse(at: Int, response: Response<[Photo]>) {
        critical(mutex: &resourcesMutex) {
            responses.append(response)
            photos.append(contentsOf: response.object)
        }
    }
    
    // Remove all responses
    private func removeAllResponses() {
        critical(mutex: &resourcesMutex) {
            responses.removeAll()
            photos.removeAll()
        }
    }
    
}
