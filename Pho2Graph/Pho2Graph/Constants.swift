//
//  Constants.swift
//  Pho2Graph
//
//  Created by Andreas Hallberg on 2017-08-31.
//  Copyright © 2017 Andreas Hallberg. All rights reserved.
//

import Foundation


/// Duration for pop up effect
let photoPopUpEffectAnimationDuration = 0.2

/// Duration for image fade in
let imageFadeInDuration = 0.2

/// Number of cells per row in landscape
let cellsPerRowLandscape = 6

/// Number of cells per row in portrait
let cellsPerRowPortrait = 3

/// Number of photos to fetch per page
let photosPerPage = 30

/// Fetch new photos when there are `fetchWhenLeft` photos from the end
let fetchWhenLeft = cellsPerRowPortrait + 1

/// Delay after writing before searching
let searchDelay = 0.5

/// Prefix when searching for user
let userPrefix = "user:"
