A simple photo viewer for Unsplash.
It does not plot Vietnamese noodle soups.

Functionality: 

Grid view for thumbnails with smooth scrolling  
Search by keyword and user via "user:USERNAME" format.  
Expandable/Collapsible cells with animation  
Go to user's photos by touching the name  
Photo viewer with paging  
Zooming in on photos  
Automatic loading of more photos (from both main view and photo viewer)  

Third party libraries:  
UnsplashKit: For connectivity to Unsplash API

Notes:  
Tested on iPhone 6+  

Added method "followLink" in UnsplashKit API for easier pagination. Note: I wanted to put this in an extension to not pollute the pods, but I couldn't get it to compile due to some problems with the type inference.  
Therefore, please use the Pods in the repo instead of downloading them via `pod install`.
